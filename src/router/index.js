import Vue from 'vue'
import VueRouter from 'vue-router'
import Homepage1 from '../components/05.homepage.vue'
import News1 from '../views/News.vue'
import Artists from '../views/Aritists.vue'
import Music from '../views/Music.vue'
import Album from '../views/Album.vue'
import About from '../views/Abuout.vue'

Vue.use(VueRouter)
//路由配置
const routes = [
  {
    path: '/',
    name: 'Homepage1',
    component: Homepage1
  },
  {
    path: '/News1',
    name: 'News1',
    component: News1
  },
  {
    path: '/Artists',
    name: 'Artists',
    component: Artists
  },
  {
    path: '/Music',
    name: 'Music',
    component: Music
  },
  {
    path: '/Album',
    name: 'Album',
    component: Album
  },
  // {
  //   path: '/Album',
  //   name: 'Album',
  //   component: Album
  // },
  {
    path: '/About',
    name: 'About',
    component: About
  },
]

const router = new VueRouter({
  routes
})

export default router
