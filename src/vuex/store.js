import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex)
export default new Vuex.Store({
    // 储存数据
    state: {
        arr: [],
        musicurl: [],
    },
    //同步修改数据
    mutations: {
        setData(state, obj) {
            state.arr = obj
            // console.log(state.arr)
        },
        seturl(state, url) {
            state.musicurl = url
            console.log(state.musicurl)
        }
    },

    actions: {

    },
})