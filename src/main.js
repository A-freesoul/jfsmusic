import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './vuex/store'

//导入element
import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'



Vue.config.productionTip = false
//use Element
Vue.use(Element)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
